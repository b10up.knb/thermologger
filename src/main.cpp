#include <Arduino.h>
#include "RTClib.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <SD.h>

#define LED_GREEN A1
#define LED_RED A2

unsigned long readInterval = 10;
unsigned long blinkerInterval = 5000;
unsigned long prevBlink = 0;
unsigned long prevRead = 0;

OneWire ds18x20[] = {2, 3, 4, 5, 6, 7, 8, 9};
const int oneWireCount = sizeof(ds18x20) / sizeof(OneWire);
DallasTemperature sensor[oneWireCount];

RTC_DS3231 rtc;

File OpenFile;

const int chipSelect = 10;

void setup()
{
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);

  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_GREEN, HIGH);
  delay(1000);
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, LOW);

  Serial.begin(250000);
  Serial.println(F("\nData Logger 2021"));

  // following line sets the RTC to the date & time this sketch was compiled
  //rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  rtc.begin();
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  // Start up the library on all defined bus-wires
  Serial.println(F("Scanning onewire"));
  DeviceAddress deviceAddress;
  for (int i = 0; i < oneWireCount; i++)
  {
    sensor[i].setOneWire(&ds18x20[i]);
    sensor[i].begin();
    if (sensor[i].getAddress(deviceAddress, 0))
      sensor[i].setResolution(deviceAddress, 12);
  } 
  pinMode(SS, OUTPUT);
  Serial.print(F("Initializing SD card..."));
  if (!SD.begin(chipSelect))
  {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  // re-open the file for reading:
  OpenFile = SD.open("SAMPLING.TXT");
  if (OpenFile)
  {
    Serial.println("Sampling interval:");

    // read from the file until there's nothing else in it:
    while (OpenFile.available())
    {
      readInterval = OpenFile.parseInt();
    }
    // close the file:
    OpenFile.close();
  }
  else
  {
    // if the file didn't open, print an error:
    Serial.println(F("error opening interval config"));
  }
  if (readInterval == 0)
  {
    Serial.println(F("Failed to read interval, using 10 sec"));
  }
  else
  {

    Serial.print("Configured interval:");
    Serial.print(readInterval, DEC);
    Serial.println(F(" seconds"));
    readInterval = readInterval * 1000;
  }
  Serial.println(F(" "));
  digitalWrite(LED_GREEN, HIGH);
  digitalWrite(LED_RED, HIGH);
  delay(2500);

  // Check to see if the file exists:
  if (SD.exists("LOG.CSV"))
  {
    Serial.println(F("Log file exists, appending."));
    OpenFile = SD.open("LOG.CSV", FILE_WRITE);
    if(!OpenFile)
    {
      Serial.println(F("Failed to open log."));
       while (1)
      {
        digitalWrite(LED_RED, HIGH);
        digitalWrite(LED_GREEN, LOW);
        delay(500);
        digitalWrite(LED_RED, LOW);
        digitalWrite(LED_GREEN, LOW);
        delay(500);
      }
    }
  }
  else
  {
    Serial.println(F("Creating new log file"));
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_RED, HIGH);
    delay(2500);
    OpenFile = SD.open("LOG.CSV", FILE_WRITE);
    OpenFile.println(F("TIMESTAMP,CH1,CH2,CH3,CH4,CH5,CH6,CH7,CH8,"));
    OpenFile.flush();
    delay(500);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_RED, LOW);
    Serial.println(F("Log file created."));
  }
  digitalWrite(LED_GREEN, HIGH);
  digitalWrite(LED_RED, LOW);
}

void loop()
{
  if (millis() - prevBlink > blinkerInterval)
  {
    prevBlink = millis();
    digitalWrite(LED_GREEN, HIGH);
    delay(5);
    digitalWrite(LED_GREEN, LOW);
  }
  if (millis() - prevRead > readInterval)
  {
    prevRead = millis();
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, HIGH);
    DateTime now = rtc.now();

    Serial.print(now.year(), DEC);
    Serial.print('/');
    if (now.month() < 10)
      Serial.print(F("0"));
    Serial.print(now.month(), DEC);
    Serial.print('/');
    if (now.day() < 10)
      Serial.print(F("0"));
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    if (now.hour() < 10)
      Serial.print(F("0"));
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    if (now.minute() < 10)
      Serial.print(F("0"));
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    if (now.second() < 10)
      Serial.print(F("0"));
    Serial.print(now.second(), DEC);
    Serial.println();

    Serial.print("Requesting temperatures...");
    for (int i = 0; i < oneWireCount; i++)
    {
      sensor[i].requestTemperatures();
    }
    Serial.println("DONE");

    String dataString = "";
    dataString += String(now.year());
    dataString += String("/");
    if (now.month() < 10)
      dataString += String("0");
    dataString += String(now.month());
    dataString += String("/");
    if (now.day() < 10)
      dataString += String("0");
    dataString += String(now.day());
    dataString += String(" ");

    if (now.hour() < 10)
      dataString += String("0");
    dataString += String(now.hour());
    dataString += String(":");
    if (now.minute() < 10)
      dataString += String("0");
    dataString += String(now.minute());
    dataString += String(":");
    if (now.second() < 10)
      dataString += String("0");
    dataString += String(now.second());
    dataString += String(",");

    for (int i = 0; i < oneWireCount; i++)
    {
      float temperature = sensor[i].getTempCByIndex(0);
      Serial.print("Temperature for the sensor ");
      Serial.print(i);
      Serial.print(" is ");
      Serial.println(temperature);
      dataString += String(temperature);
      dataString += String(",");
    }
    Serial.println();

    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, LOW);

    if (OpenFile)
    {
      delay(550);
      OpenFile.println(dataString);
      OpenFile.flush();
      Serial.print(F("Line written:"));
      Serial.println(dataString);
      delay(250);
    }
    else
    {
      Serial.println(F("error opening LOG.CSV"));
      // Wait forever since we cant write data
      while (1)
      {
        digitalWrite(LED_RED, HIGH);
        digitalWrite(LED_GREEN, LOW);
        delay(500);
        digitalWrite(LED_RED, LOW);
        digitalWrite(LED_GREEN, LOW);
        delay(500);
      }
    
  }

  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, HIGH);
  delay(500);
}

if (Serial.available())
{

  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, HIGH);
  DateTime now = rtc.now();
  char byte = Serial.read();
  Serial.write(byte);
  switch (byte)
  {
  case 'y':
  {
    Serial.println(F("Enter current year"));
    Serial.setTimeout(10000);

    int year = 0;
    char raw_year[4];
    Serial.readBytesUntil(10, raw_year, 4);
    raw_year[4] = '\0';
    Serial.println(raw_year);

    year = atoi(raw_year);
    if (year > 2020 && year < 2100)
    {
      rtc.adjust(DateTime(year, now.month(), now.day(), now.hour(), now.minute(), now.second()));
      Serial.println(F("Year adjusted"));
    }
    else
      Serial.println(F("Adjust failed"));

    break;
  }

  case 'm':
  {
    Serial.println(F("Enter current month"));
    Serial.setTimeout(10000);

    int month = 0;
    char raw_month[2];
    Serial.readBytesUntil(10, raw_month, 2);
    raw_month[2] = '\0';
    Serial.println(raw_month);

    month = atoi(raw_month);
    if (month >= 1 && month <= 12)
    {
      rtc.adjust(DateTime(now.year(), month, now.day(), now.hour(), now.minute(), now.second()));
      Serial.println(F("Month adjusted"));
    }
    else
      Serial.println(F("Adjust failed"));
    break;
  }
  case 'd':
  {
    Serial.println(F("Enter current day"));
    Serial.setTimeout(10000);

    int day = 0;
    char raw_day[2];
    Serial.readBytesUntil(10, raw_day, 2);
    raw_day[2] = '\0';
    Serial.println(raw_day);

    day = atoi(raw_day);
    if (day >= 1 && day <= 31)
    {
      rtc.adjust(DateTime(now.year(), now.month(), day, now.hour(), now.minute(), now.second()));
      Serial.println(F("Day adjusted"));
    }
    else
      Serial.println(F("Adjust failed"));
    break;
  }
  case 'h':
  {
    Serial.println(F("Enter current hour"));
    Serial.setTimeout(10000);

    int hour = 0;
    char raw_hour[2];
    Serial.readBytesUntil(10, raw_hour, 2);
    raw_hour[2] = '\0';
    Serial.println(raw_hour);

    hour = atoi(raw_hour);
    if (hour >= 0 && hour <= 23)
    {
      rtc.adjust(DateTime(now.year(), now.month(), now.day(), hour, now.minute(), now.second()));
      Serial.println(F("Hour adjusted"));
    }
    else
      Serial.println(F("Adjust failed"));
    break;
  }
  case 'i':
  {
    Serial.println(F("Enter current minute"));
    Serial.setTimeout(10000);

    int minute = 0;
    char raw_minute[2];
    Serial.readBytesUntil(10, raw_minute, 2);
    raw_minute[2] = '\0';
    Serial.println(raw_minute);

    minute = atoi(raw_minute);
    if (minute >= 0 && minute <= 59)
    {
      rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour(), minute, now.second()));
      Serial.println(F("Minute adjusted"));
    }
    else
      Serial.println(F("Adjust failed"));
    break;
  }
  default:
    Serial.println(F("Invalid option,\n see README\nKey Map:\n y - years\n m - month\n d - day\n h - hours\n i - minutes"));
    break;
  }
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_GREEN, LOW);
}
}